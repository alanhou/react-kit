import React from 'react'
import DuckImage from '../assets/Duck.jpg'

const duckStyle = {
  display: 'block',
  width: '120px',
  margin: '1.5rem auto',
}

export const HomeView = () => (
  <div>
    <h4>Welcome!</h4>
    <img
      alt='This is a duck, because Redux!'
      style={duckStyle}
      src={DuckImage} />
  </div>
)

export default HomeView
