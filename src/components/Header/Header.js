import React from 'react'
import { IndexLink, Link, } from 'react-router'

export const routeActiveStyle = {
  fontWeight: 'bold',
  textDecoration: 'underline',
}

export const Header = () => (
  <div>
    <h1>React Redux Starter Kit</h1>
    <IndexLink to='/' activeStyle={routeActiveStyle}>
      Home
    </IndexLink>
    {' · '}
    <Link to='/counter' activeStyle={routeActiveStyle}>
      Counter
    </Link>
  </div>
)

export default Header
