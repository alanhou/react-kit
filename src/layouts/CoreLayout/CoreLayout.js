import React from 'react'
import Header from '../../components/Header'

const coreLayoutViewport = {
  paddingTop: '4rem',
}

export const CoreLayout = ({ children, }) => (
  <div className='container text-center'>
    <Header />
    <div style={coreLayoutViewport}>
      {children}
    </div>
  </div>
)

CoreLayout.propTypes = {
  children: React.PropTypes.element.isRequired,
}

export default CoreLayout
