write a description using this template:

### scenario (needs acceptance test)
a user should be able to calculate the sum of 2 numbers

### actions
0. press AC button
0. enter expression 1 + 2
0. press = button

### outcome
the calculator should output 3
