write a description using this template:

### scenario (needs better acceptance or unit test)
a user should be able to calculate the sum of 2 numbers

### actions
0. press AC button
0. enter 1 + 2
0. press = button

### expected outcome
the calculator should output 3

### actual outcome
the calculator outputs 1
